
Ext.define('App.view.form.FormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.form',

    saveForm: function() {
        var view = this.getView();
        var form = view.getForm();
        var grid = Ext.getCmp('users-grid');
        var store = grid.getStore();
        var record = null;

        if (form.isValid()) {

            var data = form.getValues();

            if (data.guid) {
                record = store.findRecord('guid', data.guid);
                record.set(data);
            } else {
                var user = Ext.create('App.model.Personnel', data);
                store.add(user);
            }

            view.up('window').close();
        }
    }
});

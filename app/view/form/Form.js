﻿
Ext.define('App.view.form.Form', {
    extend: 'Ext.form.Panel',
    bodyPadding: 15,
    
    xtype: 'app-form',

    controller: 'form',

    requires: [
        'Ext.form.field.Spinner',
        'Ext.form.field.Hidden' //build does not work without this 
    ],

    fieldDefaults: {
        labelAlign: 'top',
        labelStyle: 'font-weight: bold'
    },
    
    items: [
        {
            name: 'guid', 
            xtype: 'hiddenfield'
        },

        {
            xtype: 'textfield',
            width: '100%',
            allowBlank: false,
            fieldLabel: 'First Name',
            emptyText: 'First Name',
            name: 'first_name'
        },

        {
            xtype: 'textfield',
            width: '100%',
            allowBlank: false,
            fieldLabel: 'Last Name',
            emptyText: 'Last Name',
            name: 'last_name'
        }
    ],
    
    bbar: [
        {
            text: 'Сохранить',
            formBind: true, //only enabled once the form is valid
            disabled: true,
            handler: 'saveForm'
        },

        '->',

        {
            text: 'Отмена',
            handler: 'closeForm',
            handler: function(f) {
                f.up('window').close();
            }
        }
    ],
    
    initComponent: function(one, data){

        var record = this.config.record;
        this.callParent();

        var spinner = Ext.create('Ext.form.field.Spinner', {
            labelStyle: 'font-weight: bold',
            step: 1,
            labelAlign: 'top', 
            fieldLabel: 'Age',
            name: 'age',
            value: 0,
            maskRe: /\d+/,
            validator: function (v) {
                return parseInt(v) > 0 ? true : 'Age shoud be greater than zero';
            },
            onSpinUp: function() {
                this.setValue(parseInt(this.getValue()) + this.step);
            },
            onSpinDown: function() {
                var v = this.getValue() - this.step;
                if(v < 0) v = 0;
                this.setValue(v);
            }
        });

        this.add(spinner);

        if (record) this.loadRecord(record);
        
    }
    
    
});

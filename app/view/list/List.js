
Ext.define('App.view.list.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'App.store.Personnel',
        'Ext.grid.filters.Filters',
        'App.view.list.ListController'
    ],

    controller: 'list',

    id: 'users-grid',

    store: {
        type: 'personnel'
    },

    plugins: 'gridfilters',

    bbar: [
        {
            text: 'Создать',
            handler: 'addRow'
        }
    ],

    columns: [

        // {
        //     xtype: 'rownumberer'
        // },

        {
            text : '#',
            sortable : false,
            cls: 'bold-column',
            renderer : function(value, metaData, record, rowIndex)
            {
                return rowIndex + 1;
            }
        },

        {
            renderer: function() {
                return '<img src="resources/images/User.png" width="16">';
            }
        },

        { 
            text: 'First Name', 
            cls: 'bold-column',
            dataIndex: 'first_name',
            sortable: true,
            filter: {
                type: 'string'
            }
        },

        { 
            text: 'Last Name', 
            cls: 'bold-column',
            dataIndex: 'last_name',
            sortable: true,
            filter: {
                type: 'string'
            }
        },
        
        { 
            text: 'Age', 
            cls: 'bold-column',
            dataIndex: 'age',
            filter: {
                type: 'number'
            }
        },

        {
            text: 'Summary',
            cls: 'bold-column',
            dataIndex: 'summary',
            sortable: true,
            flex: 1,
            filter: {
                type: 'string'
            }
        },

        {
            text: 'Actions',
            cls: 'bold-column',
            renderer: function(value, md, record, row, col, store) {
                var actions = [
                    '<a class="edit-link" href="#edit-record-' + record.id + '">Edit</a>',
                    '<a class="delete-link" href="#delete-record-' + record.id + '">Delete</a>'
                ];

                return actions.join('&nbsp');
            }
        }
    ],

    listeners: {
        cellclick: 'cellClick',
        rowdblclick : 'editRow'
    }
});

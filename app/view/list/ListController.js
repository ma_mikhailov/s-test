
Ext.define('App.view.list.ListController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.list',

    requires: [
        'App.view.form.Form'
    ],

    cellClick: function(g, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
        var el = e.getTarget('a.delete-link');
        if (el) {
            e.preventDefault();
            Ext.Msg.confirm('Confirm', 'Are you sure?', function(choice){
                if (choice === 'yes') g.store.removeAt(rowIndex);
            }, this);
        } else {
            el = e.getTarget('a.edit-link');
            if (el) {
                e.preventDefault();
                this.showForm(g.store.getAt(rowIndex));
            }
        }
    },

    editRow: function() {
        var record = null;
        var selection = this.getView().getSelectionModel().selected.items;
        if(selection.length) record = selection[0];
        this.showForm(record);
    },

    addRow: function() {
        this.showForm(null);
    },

    showForm: function(record) {
        var wnd = Ext.create('Ext.Window', {
            height: 300,
            width: 200,
            layout: 'fit',
            border: false,
            header: false,
            modal: true,
            items: [
                Ext.create('App.view.form.Form', {record: record})
            ]
        });

        wnd.show();
    }
});

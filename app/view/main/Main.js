
Ext.define('App.view.main.Main', {
    extend: 'Ext.container.Container',
    xtype: 'app-main',

    requires: [
        'App.view.main.MainController',
        'App.view.main.MainModel',
        'App.view.list.List',
        'Ext.plugin.Viewport'
    ],

    controller: 'main',
    viewModel: 'main',

    layout: {
        type: 'border'
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    items: [{
        region: 'center',
        xtype: 'mainlist'
    }]
});

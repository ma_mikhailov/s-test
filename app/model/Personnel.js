Ext.define('App.model.Personnel', {
    extend: 'App.model.Base',

    idProperty: 'guid',

    fields: [
      { name: 'guid'},
      { name: 'name', defaultValue: { first: '', last: ''} },
      { name: 'age' },
      { name: 'last_name', type: 'string', mapping: 'name.last'},
      { name: 'first_name', type: 'string', mapping: 'name.first'},
      { name: 'email', type: 'string' },
      { 
        name: 'summary', 
        calculate: function(data) {

          var first = data.first_name;
          var last = data.last_name;

          var res = [
            first ? first[0].toUpperCase() : '',
            '.',
            last ? last[0].toUpperCase() : '',
            '.',
            ' - ',
            data.email
          ];
          return res.join('');
        }
      }
    ]
});

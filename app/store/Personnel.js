Ext.define('App.store.Personnel', {
    extend: 'Ext.data.Store',
    autoLoad: true,

    alias: 'store.personnel',

    model: 'App.model.Personnel',

    proxy: {
        type: 'ajax',
        url: '/resources/fixtures/mates.json',
        reader: {
            type: 'json'
        }
    }

});
